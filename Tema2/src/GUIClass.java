import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

/*
 * Clasa de GUI: Aici are loc crearea componentelor ce tin de GUI si aranjarea acestora in 
 * JFrame.
 */
public class GUIClass extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	public static final void triggerWindow() // cream JFrame-ul
	{
		JFrame window = new JFrame("Simulare Cozi");
		GUIClass content = new GUIClass();
		window.setContentPane(content);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(700,700);
		window.setLocation(150,50);
		window.setVisible(true);
		window.setEnabled(true);
	}
	
	JLabel title;
	JLabel oraCurenta;
	JTextField oraCurentaField;
	JLabel aparitieClienti;
	JLabel minAparitieClienti;
	JTextField minAparitieClientiField;
	JLabel maxAparitieClienti;
	JTextField maxAparitieClientiField;
	JLabel intervalServire;
	JLabel minIntervalServire;
	JTextField minIntervalServireField;
	JLabel maxIntervalServire;
	JTextField maxIntervalServireField;
	JLabel simulationTime;
	JLabel oraStartSimulare;
	JTextField oraStartSimulareField;
	JLabel oraSfarsitSimulare;
	JTextField oraSfarsitSimulareField;
	JLabel numarCase;
	JTextField numarCaseField;
	JTextArea monitoringResults;
	JLabel timeEvolution;
	JTextArea timeEvolutionArea;
	JButton startButton;
	JScrollPane scroller1;
	JScrollPane scroller2;
	
	public GUIClass()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(255,200,70));
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(1,3));
		northPanel.setBorder(new LineBorder(Color.BLACK));
		northPanel.setBackground(new Color(204,204,255));
		title = new JLabel("Simularea Cozilor de Asteptare");
		northPanel.add(title);
		oraCurenta = new JLabel("Ora curenta:");
		northPanel.add(oraCurenta);
		oraCurentaField = new JTextField();
		oraCurentaField.setEditable(false);
		northPanel.add(oraCurentaField);
		this.add(northPanel,BorderLayout.NORTH);
		
		JPanel westPanel = new JPanel();
		westPanel.setBackground(new Color(0,255,128));
		westPanel.setLayout(new GridLayout(8,1,10,10));
		westPanel.setBorder(new LineBorder(Color.BLACK));
		this.add(westPanel,BorderLayout.WEST);
		
		aparitieClienti = new JLabel("Clientii apar la un interval de:");
		westPanel.add(aparitieClienti);
		
		JPanel westPanel1 = new JPanel();
		westPanel1.setBackground(new Color(0,255,128));
		westPanel1.setLayout(new GridLayout(1,4));
		
		minAparitieClienti = new JLabel("Minimum:");
		westPanel1.add(minAparitieClienti);
		minAparitieClientiField = new JTextField();
		westPanel1.add(minAparitieClientiField);
		maxAparitieClienti = new JLabel("Maximum:");
		westPanel1.add(maxAparitieClienti);
		maxAparitieClientiField = new JTextField();
		westPanel1.add(maxAparitieClientiField);
		
		westPanel.add(westPanel1);
		
		intervalServire = new JLabel("Intervalul de procesare al clientilor:");
		westPanel.add(intervalServire);
		
		JPanel westPanel2 = new JPanel();
		westPanel2.setBackground(new Color(0,255,128));
		westPanel2.setLayout(new GridLayout(1,4));
		
		
		minIntervalServire = new JLabel("Minimum:");
		westPanel2.add(minIntervalServire);
		minIntervalServireField = new JTextField();
		westPanel2.add(minIntervalServireField);
		maxIntervalServire = new JLabel("Maximum:");
		westPanel2.add(maxIntervalServire);
		maxIntervalServireField = new JTextField();
		westPanel2.add(maxIntervalServireField);
		
		westPanel.add(westPanel2);
		
		simulationTime = new JLabel("Simularea are loc intre orele:");
		westPanel.add(simulationTime);
		
		JPanel westPanel3 = new JPanel();
		westPanel3.setBackground(new Color(0,255,128));
		westPanel3.setLayout(new GridLayout(1,4));
		
		oraStartSimulare = new JLabel("Ora inceput:");
		westPanel3.add(oraStartSimulare);
		oraStartSimulareField = new JTextField();
		westPanel3.add(oraStartSimulareField);
		oraSfarsitSimulare = new JLabel("Ora sfarsit:");
		westPanel3.add(oraSfarsitSimulare);
		oraSfarsitSimulareField = new JTextField();
		westPanel3.add(oraSfarsitSimulareField);
		
		westPanel.add(westPanel3);
		
		JPanel westPanel4 = new JPanel();
		westPanel4.setBackground(new Color(0,255,128));
		westPanel4.setLayout(new GridLayout(1,2));
		
		numarCase = new JLabel("Numar de case:");
		westPanel4.add(numarCase);
		numarCaseField = new JTextField();
		westPanel4.add(numarCaseField);
		
		westPanel.add(westPanel4);
		
		JPanel westPanel5 = new JPanel();
		westPanel5.setLayout(new GridLayout());
		
		monitoringResults = new JTextArea("");
		monitoringResults.setBorder(new LineBorder(Color.RED));
		westPanel5.add(monitoringResults);
		scroller1 = new JScrollPane(monitoringResults);
		scroller1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		westPanel5.add(scroller1);
	
		westPanel.add(westPanel5);
		
		JPanel southPanel = new JPanel();
		southPanel.setBackground(new Color(255,204,153));
		southPanel.setBorder(new LineBorder(Color.BLACK));
		this.add(southPanel,BorderLayout.SOUTH);
		
		startButton = new JButton("Start Simulare!");
		startButton.addActionListener(this);
		startButton.setBackground(new Color(0,204,102));
		southPanel.add(startButton);
		
		JPanel eastPanel = new JPanel();
		eastPanel.setLayout(new GridLayout(2,1));
		eastPanel.setBackground(new Color(0,255,128));
		eastPanel.setBorder(new LineBorder(Color.BLACK));
		this.add(eastPanel,BorderLayout.CENTER);
		
		timeEvolution = new JLabel("Evolutia in timp a simularii:");
		eastPanel.add(timeEvolution);
		timeEvolutionArea = new JTextArea();
		Font f = new Font("VERDANA", Font.BOLD, 12);
		timeEvolutionArea.setFont(f);
		timeEvolutionArea.setBorder(new LineBorder(Color.RED));
		eastPanel.add(timeEvolutionArea);
		scroller2 = new JScrollPane(timeEvolutionArea);
		scroller2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		eastPanel.add(scroller2);
	}
	
	
	public void actionPerformed(ActionEvent evt)
	{
		String s = evt.getActionCommand();
		if(s.equals("Start Simulare!"))
		{
			doSimulare();
		}
	}
	
	public synchronized void doSimulare()
	{
		boolean ok = true;
		try
		{
			if(Integer.parseInt(numarCaseField.getText()) > 7)
			{
				throw new IllegalArgumentException("Prea multe case!");
			}
			else if((Double.parseDouble(oraStartSimulareField.getText()) < 0) || (Double.parseDouble(oraStartSimulareField.getText()) > 24)) 
			{
				throw new IllegalArgumentException("Ora de simulare invalida!");
			}
			else if((Double.parseDouble(oraSfarsitSimulareField.getText()) < 0) || (Double.parseDouble(oraSfarsitSimulareField.getText()) > 24)) 
			{
				throw new IllegalArgumentException("Ora de simulare invalida!");
			}
			else if((Double.parseDouble(oraSfarsitSimulareField.getText())) <= Double.parseDouble(oraStartSimulareField.getText()))
			{
				throw new IllegalArgumentException("Ora de simulare invalida!");
			}
			else if((Integer.parseInt(maxAparitieClientiField.getText())) <= Integer.parseInt(minAparitieClientiField.getText()))
			{
				throw new IllegalArgumentException("Invalid input!!");
			}
			else if(Integer.parseInt(minAparitieClientiField.getText()) < 1000)
			{
				throw new IllegalArgumentException("Acest field nu poate fi mai mic de 1000 milisecunde!");
			}
			else if((Integer.parseInt(maxIntervalServireField.getText())) <= Integer.parseInt(minIntervalServireField.getText()))
			{
				throw new IllegalArgumentException("Invalid input!!");
			}
			
			
		}
		catch(IllegalArgumentException ex)
		{
			JFrame errorFrame = new JFrame("Eroare");
			JPanel errorPanel = new JPanel();
			JLabel errorLabel = new JLabel(ex.getMessage());
			errorPanel.add(errorLabel);
			errorFrame.setContentPane(errorPanel);
			errorFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			errorFrame.setSize(200,115);
			errorFrame.setLocation(400,400);
			errorFrame.setVisible(true);
			errorFrame.setEnabled(true);
			ok = false;
		}
		
		if(ok)
		{
			oraCurentaField.setText(oraStartSimulareField.getText());
			Simulation sim = new Simulation(this);
			Thread t = new Thread(sim);
			t.start(); // pornirea simularii are loc dupa apasarea butonului de "Start"
		}
		
	}
	

}
