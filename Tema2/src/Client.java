/*
 *  Clasa client => Reprezinta un simplu client cu datele sale aferente
 */
public class Client {
	
	private int idClient; // id-ul clientului
	private double arrivalTime; // timpul de sosire al clientului la coada
	private int waitTime; // timpul petrecut de client de la momentul sosirii in coada si pana ajunge la casier
	private int processingTime; // timpul necesar procesarii clientului
	private int departureTime; // momentul plecarii clientului din coada
	
	public Client(int id, double arrival, int processing) // initializarea variabilelor instanta
	{
		this.idClient = id;
		this.arrivalTime = arrival;
		this.processingTime = processing;
		this.waitTime = 0;
		this.departureTime = 0;
	}
	
	public int getId()
	{
		return this.idClient;
	}
	
	public void setId(int newId)
	{
		this.idClient = newId;
	}
	
	public double getArrival()
	{
		return this.arrivalTime;
	}
	
	public void setArrival(double newArrival)
	{
		this.arrivalTime = newArrival;
	}
	
	public int getProcessing()
	{
		return this.processingTime;
	}
	
	public void setProcessing(int newProcessing)
	{
		this.processingTime = newProcessing;
	}
	
	public int getDeparture()
	{
		return this.departureTime;
	}
	
	public void setDeparture(int newDeparture)
	{
		this.departureTime = newDeparture;
	}
	
	public int getWait()
	{
		return this.waitTime;
	}
	
	public void setWait(int newWait)
	{
		this.waitTime = newWait;
	}
	
}
