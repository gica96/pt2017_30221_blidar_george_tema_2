import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * Clasa Coada => Clasa ce reprezinta o coada in sine. Ca si variabile instanta contine cateva date
 * utile fiecarei cozi: lista de clienti care se afla la coada, waitingTime-ul la coada, numarul cozii,
 * numarul de clienti la coada, un String pentru a putea afisa statusul cozii si un Simulation pentru
 * a fi in permanenta legatura cu interfata grafica si cu simulatorul
 */

public class Coada implements Runnable {
	
	private ArrayBlockingQueue<Client> clientQueue;
	private AtomicInteger queueWaitingPeriod;
	private volatile boolean running;
	private int queueNumber;
	private int numberOfClients;
	private String status;
	private Simulation simHelper;
	
	public Coada(int number) // initializarea cozii
	{
		clientQueue = new ArrayBlockingQueue<Client>(150); 
		queueWaitingPeriod = new AtomicInteger(0);
		this.running = true;
		this.queueNumber = number;
		this.numberOfClients = 0;
	}
	
	public void setSim(Simulation s)
	{
		this.simHelper = s;
	}
	
	public int getNumberOfClients()
	{
		return this.numberOfClients;
	}
	
	public synchronized int getWaitingPeriod()
	{
		return(Integer.parseInt(queueWaitingPeriod.toString()));
	}
	
	public synchronized void setWaitingPeriod(int newValue)
	{
		queueWaitingPeriod.set(newValue);
	}
	
	
	public String getStatus()
	{
		return this.status;
	}
	
	public synchronized void addClient(Client newClient) // metoda de adaugarea a unui nou client la coada
	{
		try
		{
			this.clientQueue.add(newClient);
			this.numberOfClients++;
			this.queueWaitingPeriod.addAndGet(newClient.getProcessing());
		}
		catch(IllegalStateException ex)
		{
			try
			{
				Thread.sleep(clientQueue.peek().getProcessing()); // daca e plina coada, asteptam sa se elibere un loc
			}
			catch(InterruptedException e)
			{
				ex.printStackTrace();
			}
		}
	}
	
	public ArrayBlockingQueue<Client> getQueue()
	{
		return this.clientQueue;
	}
	
	public synchronized void setRunning(boolean newValue)
	{
		this.running = newValue;
	}
	
	public synchronized int getQueueNumber()
	{
		return this.queueNumber;
	}
	
	
	public void run() // metoda run => Defineste comportamentul thread-ului fiecarei cozi
	{
		while(running)
		{
			try
			{
				Client temp = this.clientQueue.poll(); // se preia primul client de la coada
				if(temp == null)
				{
					;
				}
				else
				{
					int sleepTime = temp.getProcessing();
					Thread.sleep(sleepTime); // se opreste coada pe timpul procesarii clientului
					this.numberOfClients--; // se decremenetaza nr de clienti
					int time = this.getWaitingPeriod();
					int currentTime = time - sleepTime; // se decrementeaza si waitingTime-ul la coada
					this.queueWaitingPeriod.set(currentTime); // se seteaza waitingTime-ul curent la coada
					status = "Clientul cu id-ul: " + temp.getId() + ", cu arrivalTime-ul: " + temp.getArrival() + ", cu timpul de procesare: " + temp.getProcessing() + " mS, tocmai a fost procesat de coada cu numarul: " + this.getQueueNumber() + " la ora: " + simHelper.getTime() + ". Acum coada are un waitingPeriod de: " + this.getWaitingPeriod() + " milisecunde, respectiv " + this.clientQueue.size() + " client/clienti";
					simHelper.getSimFrame().timeEvolutionArea.setText(simHelper.getSimFrame().timeEvolutionArea.getText() + status + "\n"); // se afiseaza in GUI
				}
				
			}
			catch(InterruptedException ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
}
