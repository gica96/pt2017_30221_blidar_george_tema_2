
/*
 * Clasa ce se ocupa de simulare. Ca si variabile instanta contine toate datele preluate din interfata
 * grafica, cat si altele aditionale utile la calcularea rezultatelor simularii.
 */
public class Simulation implements Runnable {
	
	private int minProcessingTime;
	private int maxProcessingTime;
	private int numberOfQueues;
	private int minClientAppear;
	private int maxClientAppear;
	private double startSimTime;
	private double endSimTime;
	private double currentTime;
	private double checkpointTime;
	private int countingTime;
	private int overTime;
	private double peakHour;
	private int maxClients;
	private int countClients;
	private GUIClass updateFrame;
	private Scheduler scheduler;
	private ClientGenerator cGenerator;
	
	public Simulation(GUIClass guiClass) // initializarea variabilelor instanta
	{
		this.minProcessingTime = Integer.parseInt(guiClass.minIntervalServireField.getText());
		this.maxProcessingTime = Integer.parseInt(guiClass.maxIntervalServireField.getText());
		this.numberOfQueues = Integer.parseInt(guiClass.numarCaseField.getText());
		this.minClientAppear = Integer.parseInt(guiClass.minAparitieClientiField.getText());
		this.maxClientAppear = Integer.parseInt(guiClass.maxAparitieClientiField.getText());
		this.startSimTime = Double.parseDouble(guiClass.oraStartSimulareField.getText());
		this.endSimTime = Double.parseDouble(guiClass.oraSfarsitSimulareField.getText());
		this.currentTime = Double.parseDouble(guiClass.oraCurentaField.getText());
		this.checkpointTime = startSimTime;
		this.countingTime = 0;
		this.overTime = 0;
		this.maxClients = Integer.MIN_VALUE;
		this.countClients = 0;
		this.updateFrame = guiClass;
		
		scheduler = new Scheduler(numberOfQueues); // crearea unui noi scheduler
		// crearea generatorului de clienti si punerea acestuia in functiune
		cGenerator = new ClientGenerator(startSimTime,minClientAppear,maxClientAppear, minProcessingTime, maxProcessingTime, endSimTime, scheduler, this);
		Thread clientGen = new Thread(cGenerator);
		clientGen.start();
		
		for(int i = 0 ; i < numberOfQueues; i++) // pornirea cozilor cretea deja cu ajutorul scheduler-ului
		{
			scheduler.getThreadByIndex(i).start();
		}
	}
	
	public double getTime()
	{
		return this.currentTime;
	}
	
	public GUIClass getSimFrame()
	{
		return this.updateFrame;
	}
	
	public void run() // metoda run -> implementeaza si descrie comportarea thread-ului ce se ocupa de simulator
	{
		while(currentTime <= endSimTime)
		{
			// verficiarea peekHour-ului -> Se parcurge fiecare coada la fiecare moment de timp
			// si se calculeaza numarul total de clienti la acel moment
			for(int k = 0; k < this.numberOfQueues; k ++)
			{
				countClients = countClients + scheduler.getCoadaByIndex(k).getNumberOfClients();
			}
			
			if(countClients > maxClients) // aici retinem momentul peekHour-ului si numarul de clienti
			{
				maxClients = countClients;
				peakHour = currentTime;
			}
			
			countClients = 0;
			
			// a trecut 1 minut => Punem ora curenta la valoarea imediat urmatoare
			// ex: 3.59 => 4.00
			if(countingTime == 60) 
			{
				checkpointTime ++;
				currentTime = checkpointTime;
				countingTime = 0;
			}
			//updatam ora
			String s = String.format("%.2f", currentTime);
			updateFrame.oraCurentaField.setText(s);
			currentTime = currentTime + 0.01;
			cGenerator.setCurrentTime(currentTime);
			countingTime++;
			
			try
			{
				Thread.sleep(1000); // a treut o secunda
			}
			catch(InterruptedException ex)
			{
				ex.printStackTrace();
			}
		
		}
		
		cGenerator.setRunning(false); // in afara while-ului => oprim generatorul
		
		// parcurgem fiecare coada pusa in functiune dupa terminarea simularii si vedem
		// care din acestea mai au inca clienti de procesat
		// Daca e goala in momentul opririi simularii, o vom inchide, altfel, vom tot astepta cate
		// o secunda pana clientii vor parasi coada respectiva
		for(int i = 0 ; i < numberOfQueues; i++)
		{
			if(scheduler.getCoadaByIndex(i).getQueue().isEmpty())
			{
				scheduler.getCoadaByIndex(i).setRunning(false);
			}
			else
			{
				while(true)
				{
					try
					{
						Thread.sleep(1000);
						overTime+=1000;
						if(scheduler.getCoadaByIndex(i).getQueue().isEmpty())
						{
							scheduler.getCoadaByIndex(i).setRunning(false);
							break;
						}
					}
					catch(InterruptedException ex)
					{
						ex.printStackTrace();
					}
				}
			}
		}
		
		// Printam pe JTextField rezultatele simularii
		updateFrame.monitoringResults.setText(updateFrame.monitoringResults.getText() + "Timpul mediu de asteptare: " + ((cGenerator.getTotalWaitTime())/(cGenerator.getNumberOfGenClients()-1)) + " milisecunde\n");
		updateFrame.monitoringResults.setText(updateFrame.monitoringResults.getText() + "Timpul mediu de procesare: " + ((cGenerator.getTotalProcessingTime())/(cGenerator.getNumberOfGenClients()-1)) + " milisecunde\n");
		
		updateFrame.monitoringResults.setText(updateFrame.monitoringResults.getText() + "Magazinul s-a inchis peste: " + overTime + " milisecunde dupa programul obisnuit!\n");
		updateFrame.monitoringResults.setText(updateFrame.monitoringResults.getText() + "Peak hour-ul a fost la: " + peakHour + " cu un numar de: " + maxClients + " clienti\n");
		
		String genClients = "S-au generat: " + (cGenerator.getNumberOfGenClients()-1) + " clienti.\n";
		updateFrame.monitoringResults.setText(updateFrame.monitoringResults.getText() + genClients);
	}
}
