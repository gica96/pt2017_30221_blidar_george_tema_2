import java.util.ArrayList;

/*
 * Clasa reprezinta simulatorul de clienti care va genera pe durata rularii magazinului clienti
 * La fel ca simulatorul, are nevoie de datele din GUI pentru a putea crea si manipula clientii,
 * un scheduler pentru a putea fi in legatura cu cozile si alte 2 variabile instanta folosite pentru
 * calculul rezultatelor simularii.
 */
public class ClientGenerator implements Runnable {
	
	private double startSim;
	private double endSim;
	private int minArrival;
	private int maxArrival;
	private double currentTime;
	private int sleepTime;
	private int minProcessing;
	private int maxProcessing;
	private int numberOfGeneratedClients;
	private volatile boolean running;
	private ArrayList<Client> generatedClients;
	private Scheduler s;
	private Simulation sim;
	private int totalWaitTime;
	private int totalProcessingTime;
	
	// initializarea var. instanta
	public ClientGenerator(double start, int min, int max, int minProcessing, int maxProcessing, double end, Scheduler s, Simulation sim)
	{
		this.startSim = start;
		this.endSim = end;
		this.minArrival = min;
		this.maxArrival = max;
		this.running = true;
		this.currentTime = startSim;
		this.minProcessing = minProcessing;
		this.maxProcessing = maxProcessing;
		this.numberOfGeneratedClients = 0;
		this.generatedClients = new ArrayList<Client>();
		this.s = s;
		this.sim = sim;
	}
	
	public int getTotalWaitTime()
	{
		return this.totalWaitTime;
	}
	
	public int getTotalProcessingTime()
	{
		return this.totalProcessingTime;
	}
	
	public synchronized void setCurrentTime(double value)
	{
		this.currentTime = value;
	}
	
	public synchronized void setRunning(boolean value)
	{
		this.running = value;
	}
	
	public int getNumberOfGenClients()
	{
		return this.numberOfGeneratedClients;
	}
	
	public ArrayList<Client> getList()
	{
		return this.generatedClients;
	}
	
	// din considerente safe, clientii generati se vor pune intr-un arrayList pentru a nu le pierde
	// evidenta in momentul in care aparitie lor ar fi foarte rapida. Metoda preia clientul din capul
	// listei si ii da remove pe urma din lista
	public Client getCurrentClient() 
	{
		if(this.generatedClients.size() > 0)
		{
			Client c = this.generatedClients.get(0);
			this.generatedClients.remove(0);
			return c;
		}
		return null;
	}
	
	public synchronized void makeClient() // metoda de creare efectiva a clientilor
	{
		Client newClient;
		int id = numberOfGeneratedClients;
		Double arrival = Double.parseDouble(Double.toString(this.currentTime));
		int processing = (int)((maxProcessing-minProcessing) * Math.random()) + minProcessing;
		newClient = new Client (id, arrival, processing);
		this.generatedClients.add(newClient);
	}
	
	// metoda are rolul de a gasi coada cea mai potrivita de adaugarea a noului client generat
	// Se parcurge fiecare coada si se calculeaza waitingPeriod-ul fiecareia. Acolo unde aceasta 
	// valoara este cea mai mica => vom returna indexul acelei cozi.
	private synchronized int findBestQueue(Coada[] c)
	{
		int minNumber = Integer.MAX_VALUE;
		int pozition = -1;
		for(int i = 0 ; i < s.getMaxNumberOfQueues(); i++)
		{
			if(c[i].getWaitingPeriod() < minNumber)
			{
				minNumber = c[i].getWaitingPeriod();
				pozition = i;
			}
		}
		return pozition;
	}
	
	
	public void run() // metoda run
	{
		while(running)
		{
			if(currentTime > endSim)
			{
				this.setRunning(false);
				break;
			}
			// vom opri generatorul de clienti pentru un timp cuprins intre minArrivalTime si maxArrivalTime
			this.sleepTime = (int)((maxArrival-minArrival) * Math.random()) + minArrival;
			try
			{
				Thread.sleep(sleepTime);
			}
			catch(InterruptedException ex)
			{
				ex.printStackTrace();
			}
			makeClient(); // cream un nou client
			numberOfGeneratedClients++; // incrementam numarul de clienti generati total
			Client c = this.getCurrentClient(); // il preluam din arraylist
			if(c!=null && c.getArrival() <= endSim)
			{
				int bestQueuePoz = findBestQueue(s.getAllQueues()); // gasim numarul cozii
				Coada bestQueue = s.getCoadaByIndex(bestQueuePoz); // gasim coada
				bestQueue.setSim(this.sim); // facem legatura cozilor cu simulatorul
				bestQueue.addClient(c); // adaugam clientul
				c.setWait(bestQueue.getWaitingPeriod() - c.getProcessing()); // ii setam waitTime-ul clientului <=> timpul petrecut de client la coada pana sa ajunga in stadiul de procesare = cati clienti are inaintea lui la coada
				c.setDeparture(c.getWait() + c.getProcessing()); // setam dupa cat timp a plecat de la coada
				totalWaitTime = totalWaitTime + c.getWait(); // timpul total de asteptare folosit pt. computarea rezultatului final
				totalProcessingTime = totalProcessingTime + c.getProcessing(); // idem ca mai sus
				String status = "Clientul cu id-ul: " + c.getId() + ", cu processingTime-ul: " + c.getProcessing() + " mS, cu waitTime-ul: " + c.getWait() + " mS, a ajuns la ora: " + c.getArrival() + " la coada: " + bestQueuePoz + ". Acum coada are un waitingPeriod de: " + bestQueue.getWaitingPeriod() + " milisecunde, respectiv " + (bestQueue.getQueue().size()+1) + " client/clienti";
				sim.getSimFrame().timeEvolutionArea.setText(sim.getSimFrame().timeEvolutionArea.getText() + status + "\n"); // afisarea pe GUI
			}
		}
		
	}
}
