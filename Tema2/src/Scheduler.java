// Clasa scheduler => Clasa ce se ocupa de pastrarea cozilor, de punerea acestor in functiune,
// avand si cateva metode utile cu ajutorul carora vom manipula cozile
public class Scheduler {
	
	private Coada[] queues;
	private Thread[] queueThread;
	private int maxNumberOfQueues;
	
	public Scheduler(int maximumNumberOfQueues) // constructorul care creeaza cozile si thread-urile lor
	{
		this.maxNumberOfQueues = maximumNumberOfQueues;
		queues = new Coada[maximumNumberOfQueues];
		queueThread = new Thread[maximumNumberOfQueues];
		for(int i = 0 ; i < maximumNumberOfQueues; i++)
		{
			queues[i] = new Coada(i);
			queueThread[i] = new Thread(queues[i]);
		}
	}
	
	public Coada[] getAllQueues()
	{
		return this.queues;
	}
	
	public Coada getCoadaByIndex(int i)
	{
		return queues[i];
	}
	
	public Thread getThreadByIndex(int i)
	{
		return queueThread[i];
	}
	
	public int getMaxNumberOfQueues()
	{
		return this.maxNumberOfQueues;
	}

}
